{
  'variables': {
    'opencv_dir%': '<(module_root_dir)'
  },
  "targets": [
    {
      "target_name": "binding",
      "sources": [ 
        "src/index.cpp",
        "src/deraw.cpp",
        "src/deraw.h"
      ],
      "cflags!" : [
        "-fno-exceptions"
      ],
      "cflags_cc!": [
        "-fno-exceptions"
      ],
      'conditions': [
        ['OS=="mac"', 
          {
            'xcode_settings': {'GCC_ENABLE_CPP_EXCEPTIONS': 'YES'},
            "libraries": ["<(opencv_dir)/src/lib-mac/libraw_r.a"]
          }
        ],
        ['OS=="win"',
          {
            "libraries": ["<(opencv_dir)/src/lib-win/libraw.lib"]
          }
        ]
      ]
    },
    {
      "target_name": "copy_dll",
      "type":"none",
      'conditions': [
        ['OS=="win"',
          {
            "copies":[{
                  'destination': '<(opencv_dir)/build/Release/',
                  'files': ['<(opencv_dir)/src/lib-win/libraw.dll','<(opencv_dir)/src/lib-win/vcruntime140_1.dll']
              }]
          }
        ]
      ]
    }
  ]
}

'use strict';

var fs = require('fs');
var os = require('os');
var path = require('path');
var root = path.resolve(__dirname, '..');

/**
 * Require binding
 */
module.exports = function() {
  var binaryPath = getBinaryPath();
  if (hasBinary(binaryPath)) {
    return require(binaryPath);
  } else {
    var buildPath = getBuildPath();
    if (hasBinary(buildPath)) {
      return require(buildPath);
    } else {
      throw new Error('Not found ' + binaryPath + ' , You can rebuild it, to directory ' + root + ',  Run `node-gyp rebuild` Command.');
    }
  }
};

// 取默认编译文件
function getBuildPath() {
  return path.join(
    root,
    'build',
    'Release',
    'binding.node'
  );
}

// 取rebuild 文件
function getBinaryPath() {
  return path.join(
    root,
    'bin',
    os.platform() + '-' + os.arch()+'-' + process.versions.modules,
    'deraw.node'
  );
}

function hasBinary(path) {
  return fs.existsSync(path);
}
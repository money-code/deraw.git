//
//  dcraw.h
//  定义图片解析缩略图功能
//
//  Created by 何泽长 on 2020/12/2.
//

static class Deraw
{

public:
  static bool thumb(char *path, char *savePath);
};

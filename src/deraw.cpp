//
//  dcraw.cpp
//  实现图片解析缩略图功能
//
//  Created by 何泽长 on 2020/12/2.
//

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <list>
#include <stdlib.h>

#include "libraw/libraw.h"
#include "deraw.h"

#ifndef LIBRAW_WIN32_CALLS
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#endif

#ifdef LIBRAW_WIN32_CALLS
#define snprintf _snprintf
#endif

#ifdef WIN32
#include <io.h>
#include <direct.h>
#define ACCESS(fileName, accessMode) _access(fileName, accessMode)
#define MKDIR(path) _mkdir(path)
#else
#define ACCESS(fileName, accessMode) access(fileName, accessMode)
#define MKDIR(path) mkdir(path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)
#endif

#define MAX_PATH_LEN 256

using namespace std;

char *customCameras[] = {
    (char *)"43704960,4080,5356, 0, 0, 0, 0,0,148,0,0, Dalsa, FTF4052C Full,0",
    (char *)"42837504,4008,5344, 0, 0, 0, 0,0,148,0,0,Dalsa, FTF4052C 3:4",
    (char *)"32128128,4008,4008, 0, 0, 0, 0,0,148,0,0,Dalsa, FTF4052C 1:1",
    (char *)"24096096,4008,3006, 0, 0, 0, 0,0,148,0,0,Dalsa, FTF4052C 4:3",
    (char *)"18068064,4008,2254, 0, 0, 0, 0,0,148,0,0,Dalsa, FTF4052C 16:9",
    (char *)"67686894,5049,6703, 0, 0, 0, 0,0,148,0,0,Dalsa, FTF5066C Full",
    (char *)"66573312,4992,6668, 0, 0, 0, 0,0,148,0,0,Dalsa, FTF5066C 3:4",
    (char *)"49840128,4992,4992, 0, 0, 0, 0,0,148,0,0,Dalsa, FTF5066C 1:1",
    (char *)"37400064,4992,3746, 0, 0, 0, 0,0,148,0,0,Dalsa, FTF5066C 4:3",
    (char *)"28035072,4992,2808, 0, 0, 0, 0,0,148,0,0,Dalsa, FTF5066C 16:9",
    NULL};

//得到文件路径的目录
string GetPathDir(string filePath)
{
  string dirPath = filePath;
  size_t p = filePath.find_last_of('/'); // mac
  if (p != -1)
  {
    dirPath.erase(p);
  }
  return dirPath;
}

//创建多级目录
void CreateMultiLevel(string dir)
{
  if (ACCESS(dir.c_str(), 00) == 0)
  {
    return;
  }

  list<string> dirList;
  dirList.push_front(dir);

  string curDir = GetPathDir(dir);
  while (curDir != dir)
  {
    if (ACCESS(curDir.c_str(), 00) == 0)
    {
      break;
    }

    dirList.push_front(curDir);

    dir = curDir;
    curDir = GetPathDir(dir);
  }

  for (auto it : dirList)
  {
    MKDIR(it.c_str());
  }
}

bool Deraw::thumb(char *path, char *savePath)
{
  LibRaw *RawProcessor = new LibRaw;
  RawProcessor->imgdata.params.custom_camera_strings = customCameras;
  putenv((char *)"TZ=UTC");

#define P1 RawProcessor->imgdata.idata
#define S RawProcessor->imgdata.sizes
#define C RawProcessor->imgdata.color
#define T RawProcessor->imgdata.thumbnail
#define P2 RawProcessor->imgdata.other
#define OUT RawProcessor->imgdata.params

  int ret;
  char thumbfn[1024];

  string pp = savePath;
  string ppp = pp.substr(0, pp.find_last_of("/") + 1);
  CreateMultiLevel(ppp);
  if ((ret = RawProcessor->open_file(path)) == LIBRAW_SUCCESS)
  {
    if ((ret = RawProcessor->unpack_thumb()) == LIBRAW_SUCCESS)
    {
      snprintf(thumbfn, sizeof(thumbfn), "%s", savePath);
      if ((ret = RawProcessor->dcraw_thumb_writer(thumbfn)) == LIBRAW_SUCCESS)
      {
        RawProcessor->recycle();
        return true;
      }
      else
      {
        fprintf(stderr, "Cannot write %s: %s\n", thumbfn, libraw_strerror(ret));
        RawProcessor->recycle();
        return false;
      }
    }
    else
    {
      fprintf(stderr, "Cannot unpack_thumb %s: %s\n", path, libraw_strerror(ret));
      RawProcessor->recycle();
      return false;
    }
  }
  else
  {
    fprintf(stderr, "Cannot open_file %s: %s\n", path, libraw_strerror(ret));
    RawProcessor->recycle();
    return false;
  }
};
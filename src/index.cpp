// hello.cc
#include <node.h>
#include "deraw.h"
#include <string>
#include <v8.h>
#include <node_api.h>
#include <iostream>

using namespace std;
using namespace v8;

using v8::Context;
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::Isolate;
using v8::Local;
using v8::Null;
using v8::Object;
using v8::String;
using v8::Value;

void thumb(const FunctionCallbackInfo<Value> &args)
{
  Isolate *isolate = args.GetIsolate();
  Local<Context> context = isolate->GetCurrentContext();

  Local<String> path_arg = Local<String>::Cast(args[0]);
  v8::String::Utf8Value utfValue(isolate, path_arg); //从js获取的String类型转换成v8的字符串类型
  char *path = *utfValue;                            //转换成c++的类型，可以作为参数传递到c++底层

  Local<String> savePath_arg = Local<String>::Cast(args[1]);
  v8::String::Utf8Value utfValue2(isolate, savePath_arg); //从js获取的String类型转换成v8的字符串类型
  char *savePath = *utfValue2;                            //转换成c++的类型，可以作为参数传递到c++底层

  Local<Function>
      cb = Local<Function>::Cast(args[2]);

  cout << "path:" << path << endl;
  cout << "savePath:" << savePath << endl;
  int arg_count = args.Length();
  cout << "args-Length:" << arg_count << endl;
  // 检查传入的参数的个数。
  if (arg_count < 2)
  {
    Local<Value> argv[1] = {String::NewFromUtf8(isolate, "解析失败").ToLocalChecked()};
    cb->Call(context, Null(isolate), 1, argv).ToLocalChecked();
  }
  cout << "call thumb" << endl;
  if (Deraw::thumb(path, savePath))
  {
    Local<Value> argv[1] = {String::NewFromUtf8(isolate, "解析成功").ToLocalChecked()};
    cb->Call(context, Null(isolate), 1, argv).ToLocalChecked();
  }
  else
  {
    Local<Value> argv[1] = {String::NewFromUtf8(isolate, "解析失败").ToLocalChecked()};
    cb->Call(context, Null(isolate), 1, argv).ToLocalChecked();
  }
}

void Initialize(Local<Object> exports)
{
  NODE_SET_METHOD(exports, "thumb", thumb);
}

NODE_MODULE(NODE_GYP_MODULE_NAME, Initialize)

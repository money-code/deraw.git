/// <reference types="node" />
export = deraw

declare namespace deraw {
  function thumb(path: string, save: string, callback: Function): void
}

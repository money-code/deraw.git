# dcraw

#### 介绍

解析摄影文件 Raw 格式

#### 软件架构

项目基于 libraw 源码进行的二次开发
https://www.libraw.org/download

#### 安装教程

1.  npm deraw
2.  yarn add deraw

#### 使用说明

目前只提供解析缩略图功能
`const deraw = require('deraw')`
`deraw.thumb('~/1.CR3', '~/1.jpg', msg => {`
`console.log(msg)`
`})`

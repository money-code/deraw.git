/*
 * Index.js
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 hezechang <hezechang@gmail.com>
 *
 * 解析raw格式文件
 */
var _dcraw = require("./scripts/binding.js")();
 
module.exports = _dcraw;
